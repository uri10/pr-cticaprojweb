<?php
/**
 * Home Controller: Controller example.

 */
class RegisterHomeController extends Controller
{
	protected $view = 'home/register.tpl';

	public function build()
	{
		$this->setLayout( $this->view );



		$this->assign('alertMail', 0);


		$model = $this->getClass('RegisterHomeModel');
		$BDD= $model->getRegister();
		$num=$model->count();

		$flag=0;
		$i=0;
		$mail2=0;
		$nom2=0;

		$submit = Filter::getString( 'submit' );
		if ($submit=="Fet!"){
			$mail = Filter::getString( 'mail' );
			$name = Filter::getString( 'nom' );
			while ($i<$num){
				if ($BDD[$i][0]==$mail){
					$flag=1;
					$mail2=1;
					$this->assign('mail', $mail2);
				}
				$i=$i+1;
			}
			if ($flag==1) {
				$i = 0;
				while ($i<$num) {
					if ($BDD[0][$i] == $name) {
						$flag=1;
						$nom2=1;
						$this->assign('nom', $nom2);
					}
					$i=$i+1;
				}
			}
			$this->assign('nom', $nom2);
			$this->assign('mail', $mail2);
			if ($flag==0){
				$this->registre();
			}
		}
	}

	/**
	 *
     */
	protected function registre()
	{
		$name = Filter::getString( 'nom' );
		$mail = Filter::getString( 'mail' );
		$pass = Filter::getString( 'pass' );
		$twitter = Filter::getString( 'tweet' );

		/*iiiii*/
		$file_upload="true";
		$file_up_size=$_FILES['file_up'][size];
		$msg="";
		if ($_FILES[file_up][size]>2000000){$msg = $msg."La imatge pesa més de 2MB. Siusplau, redueix-ne el tamany i torna-ho a intentar.<BR>";
			$file_upload="false";}
		if (!($_FILES[file_up][type] =="image/jpeg" OR $_FILES[file_up][type] =="image/gif" OR $_FILES[file_up][type] =="image/png"))
		{$msg=$msg."La imatge a penjar ha de ser JPG, PNG o GIF. Qualsevol altre tipus no està permès<BR>";
			$file_upload="false";}

		//$file_name=$_FILES[file_up][name];
		$extensio = explode("/", $_FILES[file_up][type]);
		if($extensio[1]=="jpeg"){
			$extensio[1]="jpg";
		}
		$add= "imag/uploads/".$name.".".$extensio[1]; // the path with the file name where the file will be stored

		if($file_upload=="true"){
			if(move_uploaded_file ($_FILES[file_up][tmp_name], $add)){
				header("Location: " . URL_ABSOLUTE . "/benvingut");
			}else{
				echo "Failed to upload file Contact Site admin to fix the problem";
			}
		}else{
			echo $msg;
		}
		/*iiiii*/

		$model = $this->getClass('RegisterHomeModel');
		$model->inserirUsuari($name, $mail, $pass, $twitter, "http://g16p.dev/$add");
		$destinatario = $mail;
		$asunto = "Bienvenido a Ualapop";
		$cuerpo = '
			<html>
			<body>
			<h1>Hola';
		$cuerpo = $cuerpo . $name;
		$cuerpo = $cuerpo . ' !</h1>
			<p>
			Preparat per menjar piruletes?<br />
			<b>Benvingut a la web m&eacutes mullada de totes</b>.
			<br /><a href="http://www.g16p.dev/active/';
		$cuerpo = $cuerpo . $name;
		$cuerpo = $cuerpo . '">Fes click aqui per activar el teu compte!</a>
			<br />
			<br />Una abraçada!</p>
			</body>
			</html>
			';

		//para el envío en formato HTML
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

		//dirección del remitente
		$headers .= "From: Ualapop <lollipop@uallapop.com>\r\n";

		//dirección de respuesta, si queremos que sea distinta que la del remitente
		$headers .= "Reply-To: lollipop@uallapop.com\r\n";

		//ruta del mensaje desde origen a destino
		$headers .= "Return-path: " + $mail + "\r\n";

		mail($destinatario, $asunto, $cuerpo, $headers);
		header("Location: http://g16p.dev/");
	}
	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}

}