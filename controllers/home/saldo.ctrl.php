<?php
/**
 * Home Controller: Controller example.

 */
class RegisterHomeController extends Controller
{
	protected $view = 'home/register.tpl';

	public function build()
	{
		$this->setLayout( $this->view );



		$this->assign('alertMail', 0);


		$model = $this->getClass('RegisterHomeModel');
		$BDD= $model->getRegister();
		$num=$model->count();

		$flag=0;
		$i=0;
		$mail2=0;
		$nom2=0;

		$submit = Filter::getString( 'submit' );
		if ($submit=="Fet!"){
			$mail = Filter::getString( 'mail' );
			$name = Filter::getString( 'nom' );
			while ($i<$num){
				if ($BDD[$i][0]==$mail){
					$flag=1;
					$mail2=1;
					$this->assign('mail', $mail2);
				}
				$i=$i+1;
			}
			if ($flag==1) {
				$i = 0;
				while ($i<$num) {
					if ($BDD[0][$i] == $name) {
						$flag=1;
						$nom2=1;
						$this->assign('nom', $nom2);
					}
					$i=$i+1;
				}
			}
			$this->assign('nom', $nom2);
			$this->assign('mail', $mail2);
			if ($flag==0){
				$this->registre();
			}
		}
	}

	/**
	 *
     */
	protected function registre()
	{
		$name = Filter::getString( 'nom' );
		$mail = Filter::getString( 'mail' );
		$pass = Filter::getString( 'pass' );
		$twitter = Filter::getString( 'tweet' );
		var_dump($_FILES);
		//print_r($_FILES['file_upload']);

		$model = $this->getClass('RegisterHomeModel');
		$model->inserirUsuari($name, $mail, $pass, $twitter, "http://vignette3.wikia.nocookie.net/universosteven/images/2/27/Anonymous_logo_by_viperaviator-d4bwqvn.png/revision/latest?cb=20150724052459&path-prefix=es");
		$destinatario = $mail;
		$asunto = "Bienvenido a Ualapop";
		$cuerpo = '
			<html>
			<body>
			<h1>Hola';
		$cuerpo = $cuerpo + $name;
		$cuerpo = $cuerpo + ' !</h1>
			<p>
			Preparat per menjar piruletes?<br />
			<b>Benvingut a la web m&eacutes mullada de todes</b>.
			<br /><a href="http://www.g16p.dev/active/';
		$cuerpo = $cuerpo + $name;
		$cuerpo = $cuerpo + '">Fes click aqui per activar el teu compte!</a>
			<br />
			<br />Un abrazo!</p>
			</body>
			</html>
			';

		//para el envío en formato HTML
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

		//dirección del remitente
		$headers .= "From: Ualapop <lollipop@uallapop.com>\r\n";

		//dirección de respuesta, si queremos que sea distinta que la del remitente
		$headers .= "Reply-To: lollipop@uallapop.com\r\n";

		//ruta del mensaje desde origen a destino
		$headers .= "Return-path: " + $mail + "\r\n";

		mail($destinatario, $asunto, $cuerpo, $headers);
		header("Location: http://g16p.dev/");
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}

}