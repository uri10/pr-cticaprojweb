<?php
/**
 * Home Controller: Controller example.

 */
class HomeSaldoController extends Controller
{
	protected $view = 'home/saldo.tpl';

	public function build()
	{
		$this->setLayout($this->view);

		$submit = Filter::getString( 'submit' );

		$this->assign('ok', true);
		//en el cas de que no hi hagin diners suficients per alguna cosa enviem error

		$noMoney = Session::getInstance()->get('noMoney');

		if($noMoney == true){
			$this->assign('noMoney', true);
		}else{
			$this->assign('noMoney',false);
		}

		if ($submit=="Fet!"){
			$this->verificaImport();
		}
	}

	public function verificaImport(){

		$model = $this->getClass('HomeUsuarisModel');

		$import = Filter::getFloat('import');
		if($import < 0 || $import > 100){
			$ko = true;
		}
		$ingresos = Session::getInstance()->get('ingresos');
		$email = Session::getInstance()->get('email');
		$suma = $ingresos + $import;


		if ($suma > 1000 || $suma < 0) {
			$ok = false;
			$this->assign('ok', $ok);
		} else {
			$ok = true;
			$this->assign('ok', $ok);
			Session::getInstance()->set('ingresos', $suma);
			$model -> actualitzarIngresos($suma, $email);
			header("Location: " . URL_ABSOLUTE . "/home");
		}


	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}