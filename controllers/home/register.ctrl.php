<?php
/**
 * Home Controller: Controller example.

 */
class RegisterHomeController extends Controller
{
	protected $view = 'home/register.tpl';

	public function build()
	{
		$this->setLayout( $this->view );

		$this->assign('alertMail', 0);

		$submit = Filter::getString( 'submit' );
		if ($submit=="Fet!"){
			$this->mail();
		}
	}

	protected function mail()
	{
		$mail = Filter::getString( 'mail' );
		$mail2 = Filter::getString( 'mail2' );

		if ( $mail != $mail2) {
			$this->assign('alertMail', 1);
		}
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}