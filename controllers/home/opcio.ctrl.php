<?php
/**
 * Home Controller: Controller example.

 */
class HomeLoginController extends Controller
{
	protected $view = 'home/login.tpl';

	public function build()
	{

		$this->setLayout( $this->view );

		$info = $this->getParams();

		$submit = Filter::getString( 'submit' );

		$this->assign('ok', Session::getInstance()->get('ok'));

		if ($submit=="Fet!"){
			$this->verificaUsuari();
		}

	}

	public function verificaUsuari()
	{

		$model = $this->getClass('HomeUsuarisModel');
		$email = Filter::getEmail('email');
		if(!$email){
			$email = Filter::getString( 'email' );
		}
		$psw = Filter::getString( 'psw' );

		$result = $model->verificarUsuari ($email, $psw);

		$ok = empty($result);

		$this->assign('ok',$ok);

		if(!$ok) {
			$ingresos = $model -> getIngresosByEmail($email);
			$ingresos = $ingresos[0]['ingressos'];
			$isLoged = true;
			$session = Session::getInstance();
			$session ->set('ingresos', $ingresos);
			$session ->set('email', $email);
			$session ->set('isLoged', $isLoged);
			header("Location: " . URL_ABSOLUTE . "/home");
		}
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}