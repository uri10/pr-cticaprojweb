<?php
/**
 * Home Controller: Controller example.

 */
class HomeVendorController extends Controller
{
	protected $view = 'home/vendor.tpl';

	/**
	 *
     */
	public function build()
	{
		$this->setLayout($this->view);

		$this->assign('alertMail', 0);
		$info = $this->getParams();
		$nomVenedor = str_replace('-', ' ', $info['url_arguments'][0]);

		$model = $this->getClass('ProducteHomeModel');

		$usuari = $model->getUsuari($nomVenedor);
		$this->assign('usuari', $usuari);

		$comentaris = $model->getComentarisUsu($nomVenedor);
		$count = $model->countComentarisUsu($nomVenedor);

		$email = Session::getInstance()->get('email');
		$model2 = $this->getClass('HomeUsuarisModel');
		$usuariLoguejat=$model2->getUsernameByEmail ($email);
		$this->assign('usuariLoguejat', $usuariLoguejat);

		$this->assign('URL', URL_ABSOLUTE ."/del/".$usuariLoguejat."/".$nomVenedor);
		$i = 0;
		while ($i < $count) {
			$aux[$i] = $i;
			$i = $i + 1;
		}

		$isLoged = Session::getInstance()->get('isLoged');
		$this->assign('isLoged', $isLoged);

		$this->assign('aux', $aux);
		$this->assign('comentaris', $comentaris);

		$submit = Filter::getString('submit');
		if ($submit == "comenta") {
			if ($isLoged){
				if($model->comprovaComentari($usuariLoguejat)<1){
					if($usuariLoguejat==$nomVenedor){
						echo '<script type="text/javascript">alert("' . "Oooh no pots autocomentar!" . '")</script>';
					}else{
						$this->agrega();
					}
				}else{
					echo '<script type="text/javascript">alert("' . "Oooh no pots introduir més d'un comentari!" . '")</script>';
				}
			}else{
				echo '<script type="text/javascript">alert("' . "Oooh no! Has d'estar loguejat!" . '")</script>';
			}
		}
	}

	protected function agrega()
	{
		$model = $this->getClass('ProducteHomeModel');
		$comment = Filter::getString( 'comment' );
		$info = $this->getParams();
		$email = Session::getInstance()->get('email');
		$model2 = $this->getClass('HomeUsuarisModel');
		$usuariLoguejat=$model2->getUsernameByEmail ($email);
		$this->assign('usuariLoguejat', $usuariLoguejat);
		$venedor = str_replace('-', ' ', $info['url_arguments'][0]);
		$model->inserirComentarisUsu($comment, $usuariLoguejat, $venedor);
		header("Location: http://g16p.dev/vendor/$venedor");

	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}