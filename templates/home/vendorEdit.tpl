{$modules.head}

<section class="header_text sub">
    <h4>{$usuari[0][0]}</h4>
</section>
<section class="main-content">
    <div class="row">
        <div class="span9">
            <div class="row">
                <div class="span4">
                    <a href="{$producte[0][5]}" class="thumbnail" data-fancybox-group="group1" title="{$usuari[0][0]}"><img alt="" src="{$usuari[0][5]}"></a>
                </div>
                <div class="span5">
                    <address>
                        <strong>Valoració:</strong> <span>

                            {if $usuari[0][6] < 5}
                                &#9733; &#9734; &#9734; &#9734;
                            {/if}
                            {if 5 <$usuari[0][6]}
                                {if $usuari[0][6]<10}
                                    &#9733; &#9733; &#9734; &#9734;
                                {/if}
                            {/if}
                            {if 10<$usuari[0][6]}
                                {if $usuari[0][6]<100}
                                    &#9733; &#9733; &#9733; &#9734;
                                {/if}
                            {/if}
                            {if $usuari[0][6]>= 100}
                                &#9733; &#9733; &#9733; &#9733;
                            {/if}

                        </span><br>

                </div>
                <div class="span5">
                    <form class="form-inline">
                        <label class="checkbox">
                            <strong>Comentaris:</strong>
                        </label>
                        <br/>
                        <br/>
                        {foreach from=$aux item=c}
                            <div class="tab-content" style="background-color: #ecefef; margin-top: 3px">
                                <div class="tab-pane active" style="background-color: #ecefef; margin-top: 3px"><b>{$comentaris[$c][1]}: </b>{$comentaris[$c][0]}</div>
                            </div>
                        {/foreach}
                        <br />
                        <br />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="header_text">
    <h4>DONA'NS LA TEVA OPINIÓ</h4>

    <form method="post" name="registre" onsubmit="return boto()" enctype="multipart/form-data">
        <input type="text" name="comment" placeholder="Deixa el teu comentari" width="60px">
        <button class="btn btn-inverse" type="submit" value="comenta" name="submit">comenta</button>
    </form>
    <label class="checkbox">
        <a href="http://www.w3schools.com" style="color: #9298a2;">Edita el teu comentari</a>
        <br />
        <a href="{$URL}"
           onclick="return confirm('Segur que vols eliminar el comentari?');">Elimina el comentari</a>


    </label>
    <br />
    <br />
</section>
</div>

{$modules.footer}
