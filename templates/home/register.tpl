{$modules.head}

<form method="post" class="smart-green">
    <h1>Sigues un Ualapoperu
        <span>Registra't, és gratis!</span>
    </h1>
    <label>
        <span>Nom :</span>
        <input id="nom" type="text" name="nom" placeholder="Volem saber com saludarte" required/>
    </label>
    <label>
        <span>Cognoms :</span>
        <input id="cognoms" type="text" name="cognoms" placeholder="Introdueix el teu cognoms" required/>
    </label>
    <label>
        <span>Mail :</span>
        <input id="mail" type="email" name="mail" placeholder="Introdueix el teu correu electrònic" required/>
        <input id="mail2" type="email" name="mail2" placeholder="Torna a introduir el teu correu electrònic" required/>
    </label>

    <label>
        <span>Contrassenya :</span>
        <input id="pass" type="password" name="pass" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="La contrassenya" required/>
    </label>

    <label>
        <span>Tens twitter?</span>
        <input id="tweet" type="text" name="tweet" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="Dale!" required/>
    </label>

    <label>
        </br>
        <input type="submit" name="submit" class="button" value="Fet!" />
    </label>

</form>

{if $alertMail eq 1}
    <dialog id="window">
        <h3>Sample Dialog!</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, inventore!</p>
        <button id="exit">Close Dialog
    </dialog>
{/if}

<div class="clear"></div>
{$modules.footer}
