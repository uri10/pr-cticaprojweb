{$modules.head}

<form method="post" class="smart-green" name="registre" onsubmit="return boto()" enctype="multipart/form-data">
    <h1>Sigues un Ualapoperu
        <span>Registra't, és gratis!</span>
    </h1>
    <label>
        {if $mail == 1}
            <h2 style="color: #ff0000">EL MAIL JA ESTÀ REGISTRAT</h2>
        {/if}
        {if $num == 1}
            <h2 style="color: #ff0000">EL NOM D'USUARI JA ESTÀ REGISTRAT</h2>

        {/if}
        <span>Nom* :</span>
        <input id="nom" type="text" name="nom" placeholder="Volem saber com saludarte" required/>
    </label>
    <label>
        <span>Cognoms :</span>
        <input id="cognoms" type="text" name="cognoms" placeholder="Introdueix el teu cognoms" required/>
    </label>
    <label>
        <span>Mail* :</span>
        <input id="mail" type="email" name="mail" placeholder="Introdueix el teu correu electrònic" required/>
    </label>
    <br />
    <label>
        <div class="password-container" id="pp">
            <span>Contrassenya* :</span>
            <input id="pass" type="password" name="pass" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="La contrassenya" class="strong-password" required/>
            <input id="pass2" type="password" name="pass2" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="La contrassenya" class="strong-password"  required/>

            <div class="strength-indicator">
                <div class="meter">
                </div>
            </div>
        </div>
    </label>

    <label>
        <span>Twitter* :</span>
        <input id="tweet" type="text" name="tweet" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="Dale!" required/>
    </label>

    <label>
        <span>Select image to upload:</span>
        <br />
        <br />
        <input name="file_upload" type='file' onchange="readURL(this);"/>
        <img id="blah" src="#" alt="your image" />
    </label>
    <label>
        </br>
        <input type="submit" name="submit" class="button" class="submit-button locked" value="Fet!"/>
    </label>

</form>

<div class="clear"></div>
{$modules.footer}
