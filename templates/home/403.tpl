{$modules.head}

	<!-- This is an HTML comment -->
	{* This is a Smarty comment *}


		<section  class="homepage-slider" id="home-slider" height=600px>
			<img src="http://setmanadelasolidaritat.org/wp-content/uploads/2015/10/piruletas-de-colores-1882631.jpg" alt="" />
			<div class="intro">
				<h1>Piruleta Reina</h1>
				<p><span>24€</span></p>
				<p><span>Una de les més grans del món</span></p>
			</div>
		</section>

		<h4 class="title">
			<span class="pull-left"><span class="text"><span class="line">Els 5 productes <strong>més vistos</strong></span></span></span>
		</h4>

			<div class="carousel-inner">
				<div class="active item">
					<ul class="thumbnails" style="background-color: #ffffff">
						{foreach from=$aux item=c}
							<li class="span3">
								<div class="product-box">
									<span class="sale_tag"></span>
									<p><a href="{$url.global}/prod/{$productes[$c][7]}"><img src="{$productes[$c][5]}" alt="" height="400px" width="400px"/></a></p>
									<a href="{$url.global}/prod/{$productes[$c][7]}" class="title">{$productes[$c][0]}</a><br/>
									<a href="{$url.global}/prod/{$productes[$c][7]}" class="category">{$productes[$c][1]}</a>
									<p class="time">Caduca el {$productes[$c][4]}</p>
									<p class="price">{$productes[$c][3]} €</p>
								</div>
							</li>
						{/foreach}

					</ul>
				</div>
			</div>

		<h4 class="title">
		<span class="pull-left"><span class="text"><span class="line">Últimes <strong>novetats</strong></span></span></span>
		</h4>



	
<div class="clear"></div>
{$modules.footer}