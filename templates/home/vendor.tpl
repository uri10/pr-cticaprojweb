{$modules.head}

<section class="header_text sub">
    <img class="pageBanner" src="{$url.global}/themes/images/pageBanner.png" alt="Piruleta" >
    <h4>Detalls de la piruleta</h4>
</section>
<section class="main-content">
    <div class="row">
        <div class="span9">
            <div class="row">
                <div class="span4">
                    <a href="{$producte[0][5]}" class="thumbnail" data-fancybox-group="group1" title="Description 1"><img alt="" src="{$producte[0][5]}"></a>
                </div>
                <div class="span5">
                    <address>
                        <strong>Producte:</strong> <span>{$producte[0][0]}</span><br>
                        <strong>Dies restants:</strong> <span>{$producte[0][4]}</span><br>
                        <strong>Disponiblitat:</strong> <span>{$producte[0][3]} unitats</span><br>
                    </address>
                    <h4><strong>Preu: {$producte[0][2]} €</strong></h4>
                </div>
                <div class="span5">
                    <form class="form-inline">
                        <label>Qty:</label>
                        <input type="text" class="span1" placeholder="1">
                        <button class="btn btn-inverse" type="submit">Add to cart</button>
                        <p>&nbsp;</p>
                        <label class="checkbox">
                            <strong>Descripció:</strong>
                        </label>
                        <br/>
                        <label class="checkbox">
                            {$producte[0][1]}
                        </label>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="span9">
                    <address>
                        <p>&nbsp;</p>
                        <ul style="background-color: #ffffff">
                            <li class="span1" >
                                <img src="{$usuari[0][5]}" alt="" class="thumbnail" data-fancybox-group="group1" title="Description 2">
                            </li>
                        </ul>
                        <strong>Usuari:</strong> <span> {$usuari[0][0]}</span><br>
                        <strong>Valoració:</strong> <span>

                            {if $usuari[0][6] < 5}
                                &#9733; &#9734; &#9734; &#9734;
                            {/if}
                            {if 5 <$usuari[0][6]}
                                {if $usuari[0][6]<10}
                                    &#9733; &#9733; &#9734; &#9734;
                                {/if}
                            {/if}
                            {if 10<$usuari[0][6]}
                                {if $usuari[0][6]<100}
                                    &#9733; &#9733; &#9733; &#9734;
                                {/if}
                            {/if}
                            {if $usuari[0][6]>= 100}
                                &#9733; &#9733; &#9733; &#9733;
                            {/if}

                        </span>
                    </address>


                </div>
</section>

<section class="header_text">
    <section class="header_text sub">
        <h4>COMENTA</h4>
    </section>
    <form method="post" name="registre" onsubmit="return boto()" enctype="multipart/form-data">
    <input type="text" name="comment" placeholder="Deixa el teu comentari" width="60px">
    <button class="btn btn-inverse" type="submit" value="comenta" name="submit">comenta</button>
    </form>
    <br />
    <br />
    {foreach from=$aux item=c}
        <div class="tab-content">
            <div class="tab-pane active" style="background-color: #20D7BF"><b>{$comentaris[$c][2]}: </b>{$comentaris[$c][0]}</div>
        </div>
    {/foreach}

</section>

{$modules.footer}
