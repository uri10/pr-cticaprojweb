{$modules.head}

<section class="header_text sub">
    <h4>{$usuari[0][0]}</h4>
</section>
<section class="main-content">
    <div class="row">
        <div class="span9">
            <div class="row">
                <div class="span4">
                    <a href="{$usuari[0][5]}" class="thumbnail" data-fancybox-group="group1" title="{$usuari[0][0]}"><img alt="" src="{$usuari[0][5]}"></a>
                </div>
                <br />
                <div class="span5">
                    <address>
                        <strong>Valoració:</strong> <span>

                            {if $usuari[0][6] < 5}
                                &#9733; &#9734; &#9734; &#9734;
                            {/if}
                            {if 5 <$usuari[0][6]}
                                {if $usuari[0][6]<10}
                                    &#9733; &#9733; &#9734; &#9734;
                                {/if}
                            {/if}
                            {if 10<$usuari[0][6]}
                                {if $usuari[0][6]<100}
                                    &#9733; &#9733; &#9733; &#9734;
                                {/if}
                            {/if}
                            {if $usuari[0][6]>= 100}
                                &#9733; &#9733; &#9733; &#9733;
                            {/if}

                        </span><br>

                </div>
                <div class="span5">
                    <form class="form-inline">
                        <label class="checkbox">
                            <strong>Edita comentari fet al venedor {$usuari[0][0]}:</strong>
                        </label>
                        <br/>
                        <input type="text" style="background-color: #ecefef; margin-top: 3px; width: 400px;" name="comment" value="{$comentari}" width="60px">
                        <br />
                        <br />
                        <button type="submit" value="comenta" name="submit">modifica</button>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

{$modules.footer}
