{$modules.head}

<section class="header_text sub">
    <h4>ELS TEUS COMENTARIS</h4>
</section>
<section class="main-content">
    <div class="row">
        <div class="span9">
            <div class="row">
                <div class="span4">
                    <a href="{$usuari[0][5]}" class="thumbnail" data-fancybox-group="group1" title="{$usuari[0][0]}"><img alt="" src="{$usuari[0][5]}"></a>
                </div>
                <div class="span5">
                    <form class="form-inline">
                        <label class="checkbox">
                            <strong>Comentaris fets a venedors:</strong>
                        </label>
                        <br/>
                        {foreach from=$countV item=v}
                            <div class="tab-content" style="background-color: #ecefef; margin-top: 3px">
                                <div class="tab-pane active" style="background-color: #ecefef; margin-top: 3px"> <i>{$commentV[$v][2]} </i> <b><a href="{$global.url}/vendor/{$commentV[$v][1]}">{$commentV[$v][1]}:</a> </b>{$commentV[$v][0]}</div>
                            </div>
                        {/foreach}
                        <br />
                    </form>
                </div>
                <div class="span5">
                    <form class="form-inline">
                        <label class="checkbox">
                            <strong>Comentaris fets a productes:</strong>
                        </label>
                        <br/>
                        {foreach from=$countP item=c}
                            <div class="tab-content" style="background-color: #ecefef; margin-top: 3px">
                                <div class="tab-pane active" style="background-color: #ecefef; margin-top: 3px"><i>{$commentP[$c][2]} </i> <b>{$commentP[$c][1]}: </b>{$commentP[$c][0]}</div>
                            </div>
                        {/foreach}
                        <br />
                        <br />
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

{$modules.footer}
