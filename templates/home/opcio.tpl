{$modules.head}

<form method="post" class="smart-green" name="registre" onsubmit="return regProd()" enctype="multipart/form-data">
    <h1>Enregistra un nou producte !</h1>
    {if $ingres == 1}
        <h2 style="color: #ff0000">NO TENS SUFICIENTS DINERS</h2>
    {/if}
    <label>
        <label>Nom del producte: </label>
        <input id="nom" type="text" name="nom" placeholder="Nom del producte" required/>
    </label>

    <label>
        <label>Descripció del producte : </label>
        <textarea id="descripcio" type="text" name="descripcio" required/>Introdueix la descripcio</textarea>
    </label>

    <label>
        <label>Preu de venda :</label>
        <input id="preu" type="number" name="preu" step="0.1" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="Preu del producte" required/>
    </label>

    <label>
        <label>Stock :</label>
        <input id="stock" type="number" name="stock" step="1" style="color: #555; height: 30px;line-height:15px;width: 100%;padding: 0px 0px 0px 10px;margin-top: 2px;border: 1px solid #E5E5E5;background: #FBFBFB;outline: 0;-webkit-box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);box-shadow: inset 1px 1px 2px rgba(238, 238, 238, 0.2);font: normal 14px/14px Arial, Helvetica, sans-serif;" placeholder="Stock del producte" required/>
    </label>

    <label>
        <label>Data de caducitat :</label>
        <input id="caducitat" type="date" name="caducitat" min="today" data-date-startdate="{$startdate}" required/>
    </label>

    <label>
        <label>
            <span>Imatge del producte:</span>
            <br />
            <br />
            <input name="file_up" type='file' onchange="readURL(this);"/>
            <img id="blah" src="#" alt="your image" />
        </label>
    </label>

    <label>
        </br>
        <input type="submit" name="submit" class="button" class="submit-button locked" value="Fet!" />
    </label>

</form>

<div class="clear"></div>
{$modules.footer}